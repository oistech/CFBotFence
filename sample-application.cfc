component displayname="My Application" output="false" {

    this.name = "MyApplicationName";
    
	public boolean function onApplicationStart() {
		application.CFBF = new CFBotFence.cfbf();
		
		return true;
	} //onApplicationStart()

	public void function onSessionStart() { 

	    return; 
	} // onSessionStart()
	
	public boolean function onRequestStart(required string thePage)  {

		// RUN CFBotFence
		if( !structKeyExists(application, "CFBF") || url.keyExists("reInitCFBF") ){
			lock timeout="2" scope="application" type="exclusive" { application.CFBF = new CFBotFence.cfbf(); }	
		}
		if( application.CFBF.onRequestStart() ){
			location(application.CFBF.redirectURL, "false", "401");
			abort;
		}
		
	    return true; 
	} // onRequestStart()


	public void function onRequest(required string thePage) { 
        
	    include arguments.thePage runonce=true; 
	    return; 
	} // onRequest()

	public void function onError(required any exception, required string eventName) {
		// RUN CFBotFence
		if( isDefined("application") ){
			if( !structKeyExists(application, "CFBF") || url.keyExists("reInitCFBF") ){
				lock timeout="2" scope="application" type="exclusive" { application.CFBF = new CFBotFence.cfbf(); }	
			}
			if( application.CFBF.onError() ){
				location(application.CFBF.redirectURL, "false", "401");
				abort;
			}
		}else{
			CFBF = new CFBotFence.cfbf();
			if( CFBF.onError() ){
				location(CFBF.redirectURL, "false", "401");
				abort;
			}
		}

		cfmail( to = "you@yourdomain.tld", from = "SITE-ERRORS@yourdomain.tld", subject = "#CGI.Server_Name# ERROR", type = "html" ){ 
			WriteOutput( "<div style='background-color: white !important; padding: 10px 10px 10px 10px; margin: 10px 10px 10px 10px;'>" ); 
				writeOutput("<h2>Arguments</h2><br>");
				writeDump(arguments);
				writeOutput("<h2>session</h2><br>");
				try { writeDump(session); } catch (any e) { writeDump(e); }
				writeOutput("<h2>application</h2><br>");
				try { writeDump(application); } catch (any e) {writeDump(e);}
				writeOutput("<h2>cgi</h2><br>");
				writeDump(cgi);
				writeOutput("<h2>url</h2><br>");
				writeDump(url);
				writeOutput("<h2>form</h2><br>");
				writeDump(form);
				writeOutput("<h2>variables</h2><br>");
				writeDump(variables);
				writeOutput("<h2>request</h2><br>");
				writeDump(request);
				writeOutput("<h2>server</h2><br>");
				writeDump(server);
			writeOutput("</div>");
		}
        
		location("/error/", "false", "301");
		abort;
			
	    return; 
	} // onError()



} // cfcomponent