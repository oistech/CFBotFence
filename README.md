# CFBotFence

ColdFusion"integration" with BotFence ([https://www.servolutions.com/botfence.htm](https://www.servolutions.com/botfence.htm))

"BotFence automatically blocks IP addresses with hacking attempts on your windows server services (rdp, FTP, SQL-Server) using the Windows firewall."

Utilizing the built in BotFence SOAP functionality CFBotFence allows ColdFusion to submit IP addresses to be banned and BotFence manages everything else.
