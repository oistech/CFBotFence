component  displayname="CFBotFence" hint="Integrates BotFence SOAP addAttack function with CF" output="false" {
	
	this.soapURL = "http://localhost:8733/Design_Time_Addresses/BotFenceService/BotFenceWFCService/?wsdl";
	this.cfLogFile = "CFBotFence";
	this.cfLog = true;
	this.redirectURL = "/error";
	this.banUrlStrings = [ 
				"cgi.redirect_status_env", 
				"redirect_status_env", 
				"allow_url_include", 
				"open_basedir", 
				"auto_prepend_file", 
				"disable_functions" 
			];
	this.sqlInjectRegex = "(SELECT\s[\w\*\)\(\,\s]+\sFROM\s[\w]+)|(UPDATE\s[\w]+\sSET\s[\w\,\'\=]+)|(INSERT\sINTO\s[\d\w]+[\s\w\d\)\(\,]*\sVALUES\s\([\d\w\'\,\)]+)|(DELETE\sFROM\s[\d\w\'\=]+)|(DROP\sTABLE\s[\d\w\'\=]+)";
	
	public void function init( struct settingsOveride = {} ) {
		// overide settings or add settings
		for (key in settingsOveride) {
		   this[key] = settingsOveride[key];
		}
		
	} // init()
	
	public boolean function onRequestStart() description="should be run at top of application.cfc onRequestStart()" hint="returns true if page should be stopped and redirected, false if the request should continue" {
		var checkStatus = { "ban": false, "banNow": false, "banRule": "CFBF-DEFAULT" };
		
		// run checks
		checkStatus = check_URL(checkStatus);
		
		if( checkStatus.ban && checkStatus.banNow  ){
			// loop 10 times and post in order to ban immediatly
			for (i = 1; i <= 10; i++) {
				postAddAttack({ "username": checkStatus.banRule });
			}
			return true;
		}else if ( checkStatus.ban ){
			postAddAttack({ "username": checkStatus.banRule });
			return false;
		}else{
			return false;
		}
		
	} // onRequestStart()
	
	public boolean function onError(){
		var checkStatus = { "ban": false, "banNow": false, "banRule": "CFBF-DEFAULT" };
		
		// run checks
		checkStatus = check_URL(checkStatus);
		
		if( checkStatus.ban && checkStatus.banNow  ){
			// loop 10 times and post in order to ban immediatly
			for (i = 1; i <= 10; i++) {
				postAddAttack({ "username": checkStatus.banRule });
			}
			return true;
		}else if ( checkStatus.ban ){
			postAddAttack({ "username": checkStatus.banRule });
			return false;
		}else{
			return false;
		}
	} // onError()
	
	public void function postAddAttack( required struct attackData ){
		// add IP if not exists
		if( !attackData.keyExists("ip") ){
			attackData["ip"] = cgi.REMOTE_ADDR;
		}
		// add TimeStamp if not exists
		if( !attackData.keyExists("ts") ){
			var ts = dateConvert( "local2utc", now() );
			attackData["ts"] = dateFormat( ts, "yyyy-mm-dd" ) & "T" & timeFormat( ts, "HH:mm:ss" );
		}
		// add target if not specified
		if( !attackData.keyExists("target") ){
			attackData["target"] = "CFBF [" & cgi.SERVER_NAME & "]";
		}
		// add username if not specified
		if( !attackData.keyExists("username") ){
			attackData["username"] = "CFBF-DEFAULT";
		}
		// create soapRequestXML
		var soapRequestXML = '<Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/">
			    <Body>
			        <AddAttack xmlns="http://tempuri.org/">
			            <IP>#attackData["ip"]#</IP>
			            <timestamp>#attackData["ts"]#</timestamp>
			            <Target>#attackData["target"]#</Target>
			            <Username>#attackData["username"]#</Username>
			        </AddAttack>
			    </Body>
			</Envelope>';
		// POST the request
		cfhttp(method="POST", charset="utf-8", url=this.soapURL, result="soapResponse") {
		    cfhttpparam( name="SOAPAction", type="header", value="http://tempuri.org/IBotFenceWFCService/AddAttack");
		    cfhttpparam( name="accept-encoding", type="header", value="no-compression");
		    cfhttpparam( type="xml", value="#trim( soapRequestXML )#" );
		}
		
		// log attack info in CF Administrator
		if( this.cfLog ){
			var cfLogText = attackData["target"] & " | " & attackData["ip"] & " | " &  attackData["username"];
			writeLog(text=cfLogText, type="warning", application="yes", file=this.cfLogFile );
		}
	} // postAddAttack()
	
	
	/* START CHECK FUNCTIONS */
	private struct function check_URL( required struct checkStatus ){
		if( checkStatus.ban && checkStatus.banNow ){
			// already an immediate ban so just return
			return checkStatus;
			abort;
		}
		
		// LOOP OVER URL KEY/VALUE
		for( key IN url ){
			
			// CHECK: URL-PHP-INJECT (IMMEDIATE BAN)
			for( str IN this.banUrlStrings ){
				if( findNoCase(str, key ) ){
					checkStatus.banNow = true;
					checkStatus.ban = true;
					checkStatus.banRule = "URL-PHP-INJECT";
					return checkStatus;
					abort;
				}else if( findNoCase(str, url[key] ) ){
					checkStatus.banNow = true;
					checkStatus.ban = true;
					checkStatus.banRule = "URL-PHP-INJECT";
					return checkStatus;
					abort;
				}
			}
			
			// CHECK: URL-SQL-INJECT ((IMMEDIATE BAN))
			if( isSimpleValue( url[key] ) ){
				if( refindnocase( this.sqlInjectRegex, url[key] ) ){
					checkStatus.banNow = true;
					checkStatus.ban = true;
					checkStatus.banRule = "URL-SQL-INJECT";
					return checkStatus;
					abort;
				}
			}
			if( refindnocase( this.sqlInjectRegex, key ) ){
				checkStatus.banNow = true;
				checkStatus.ban = true;
				checkStatus.banRule = "URL-SQL-INJECT";
				return checkStatus;
				abort;
			}
			
			
			
		} // END - for key in url
		
		
		
		return checkStatus;
	} // check_URL( checkStatus )
	
	
	/* END CHECK FUNCTIONS */
	
	
	
} // component